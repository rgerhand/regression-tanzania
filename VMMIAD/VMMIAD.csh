rm commands.tmp
echo 'options C_COMPILER_CFG_SOURCE PY_CONFIGURATOR' >> commands.tmp
echo 'options C_COMPILER_FAMILY_NAME GNU_Native' >> commands.tmp
echo 'options C_COMPILER_HIERARCHY_STRING GNU Native_6.3_C' >> commands.tmp
echo 'options C_COMPILER_PY_ARGS --lang c --version 6.3' >> commands.tmp
echo 'options C_COMPILER_TAG GNU_C_63' >> commands.tmp
echo 'options C_COMPILER_VERSION_CMD gcc --version' >> commands.tmp
echo 'options C_COMPILE_CMD gcc -c -g -std=gnu++98' >> commands.tmp
echo 'options C_DEBUG_CMD gdb' >> commands.tmp
echo 'options C_DEFINE_LIST MAX_MSG_NAME_SIZE' >> commands.tmp
echo 'options C_EDG_FLAGS -w --gcc --gnu_version 60300' >> commands.tmp
echo 'options C_LINKER_VERSION_CMD ld --version' >> commands.tmp
echo 'options C_LINK_CMD gcc -g' >> commands.tmp
echo 'options C_PREPROCESS_CMD gcc -E -C' >> commands.tmp
echo 'options VCAST_ASSEMBLY_FILE_EXTENSIONS asm s' >> commands.tmp
echo 'options VCAST_COLLAPSE_STD_HEADERS COLLAPSE_SYSTEM_HEADERS' >> commands.tmp
echo 'options VCAST_COMMAND_LINE_DEBUGGER TRUE' >> commands.tmp
echo 'options VCAST_DISABLE_STD_WSTRING_DETECTION TRUE' >> commands.tmp
echo 'options VCAST_DISPLAY_UNINST_EXPR FALSE' >> commands.tmp
echo 'options VCAST_ENABLE_FUNCTION_CALL_COVERAGE FALSE' >> commands.tmp
echo 'options VCAST_ENVIRONMENT_FILES ' >> commands.tmp
echo 'options VCAST_HAS_LONGLONG TRUE' >> commands.tmp
echo 'options VCAST_PREPROCESS_PREINCLUDE $(VECTORCAST_DIR)/DATA/gnu_native/intrinsics.h' >> commands.tmp
echo 'options VCAST_TYPEOF_OPERATOR TRUE' >> commands.tmp
echo 'options WHITEBOX YES' >> commands.tmp
echo 'clear_default_source_dirs ' >> commands.tmp
echo 'options TESTABLE_SOURCE_DIR /home/r4/bombardier_tanzania/cbr2_tz/CBR2_TZ/implementation/fspu/fspa/source/vmmiAd/' >> commands.tmp
echo 'options TESTABLE_SOURCE_DIR /home/r4/bombardier_tanzania/cbr2_tz/CBR2_TZRelease/implementation/fspu/fspa/source/util/' >> commands.tmp
echo 'options TESTABLE_SOURCE_DIR /home/r4/bombardier_tanzania/cbr2_tz/CBR2_TZRelease/implementation/fspu/fspa/source/sem/' >> commands.tmp
echo 'options TESTABLE_SOURCE_DIR /home/r4/bombardier_tanzania/cbr2_tz/CBR2_TZRelease/implementation/fspu/fspa/source/duf/' >> commands.tmp
echo 'options TESTABLE_SOURCE_DIR /home/r4/bombardier_tanzania/cbr2_tz/CBR2_TZRelease/implementation/fspu/fspa/source/oma/' >> commands.tmp
echo 'options TESTABLE_SOURCE_DIR /home/r4/bombardier_tanzania/cbr2_tz/CBR2_TZRelease/implementation/fspu/fspa/source/iaa/' >> commands.tmp
echo 'options TESTABLE_SOURCE_DIR /home/r4/bombardier_tanzania/cbr2_tz/CBR2_TZRelease/implementation/fspu/fspa/source/version/' >> commands.tmp
echo 'options TESTABLE_SOURCE_DIR /home/r4/bombardier_tanzania/cbr2_tz/CBR2_TZRelease/implementation/fspu/fspa/source/cycl/' >> commands.tmp
echo 'options TESTABLE_SOURCE_DIR /home/r4/bombardier_tanzania/cbr2_tz/CBR2_TZ/distribution/include/' >> commands.tmp
echo 'options TESTABLE_SOURCE_DIR /home/r4/bombardier_tanzania/cbr2_tz/Headers/' >> commands.tmp
echo 'options TESTABLE_SOURCE_DIR /home/r4/bombardier_tanzania/cbr2_tz/CBR2_TZ/implementation/fspu/fspa/include/' >> commands.tmp
echo 'environment build VMMIAD.env' >> commands.tmp
echo '-eVMMIAD tools script run VMMIAD.tst' >> commands.tmp
echo '-eVMMIAD execute batch' >> commands.tmp
echo '-eVMMIAD tools import_coverage VMMIAD.cvr' >> commands.tmp
echo '-eVMMIAD reports custom management VMMIAD_management_report.html' >> commands.tmp
$VECTORCAST_DIR/clicast  -lC tools execute commands.tmp true
