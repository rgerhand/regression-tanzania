-- VectorCAST 20.sp2 (07/21/20)
-- Test Case Script
-- 
-- Environment    : TZUTIL
-- Unit(s) Under Test: tzUtilA tzUtilLoggerA
-- 
-- Script Features
TEST.SCRIPT_FEATURE:C_DIRECT_ARRAY_INDEXING
TEST.SCRIPT_FEATURE:CPP_CLASS_OBJECT_REVISION
TEST.SCRIPT_FEATURE:MULTIPLE_UUT_SUPPORT
TEST.SCRIPT_FEATURE:REMOVED_CL_PREFIX
TEST.SCRIPT_FEATURE:MIXED_CASE_NAMES
TEST.SCRIPT_FEATURE:STATIC_HEADER_FUNCS_IN_UUTS
TEST.SCRIPT_FEATURE:VCAST_MAIN_NOT_RENAMED
--

-- Unit: tzUtilA

-- Subprogram: tzUtilInitA

-- Test Case: tzUtilInitA_tzLogSetLevelStrA
TEST.UNIT:tzUtilA
TEST.SUBPROGRAM:tzUtilInitA
TEST.NEW
TEST.NAME:tzUtilInitA_tzLogSetLevelStrA
TEST.NOTES:
<userTags> 
<History> 
2022-03-25	Rafal Gerhand   	Initail creation in CBR2_TZ_220210_1
</History> 
  
<Description> 
This test case verifies the execution of function tzUtilInitA() when:
-value of the "function" parameter is set to "tzLogSetLevelStrA"
</Description> 

<Prerequisites> 
None
</Prerequisites> 
  
<Given> 
As per the EQUIVALENCE CLASS
</Given> 
  
<Expected> 
Verify that: 
- function utilRegisterSetLogLevelFunctionA() is called with name and function parameters with value cbrtz and tzLogSetLevelStrA.
</Expected> 
  
<Requirement> 
Functional 
</Requirement> 
  
<EquivalenceClass> 
None
</EquivalenceClass> 
  
<Usage> 
Functional
</Usage> 
</userTags> 
TEST.END_NOTES:
TEST.VALUE:uut_prototype_stubs.utilRegisterSetLogLevelFunctionA.function:tzLogSetLevelStrA
TEST.EXPECTED:uut_prototype_stubs.utilRegisterSetLogLevelFunctionA.name:"cbrtz"
TEST.EXPECTED:uut_prototype_stubs.utilRegisterSetLogLevelFunctionA.function:tzLogSetLevelStrA
TEST.END

-- Unit: tzUtilLoggerA

-- Subprogram: findModuleIdByNameA

-- Test Case: findModuleIdByNameA_EMPTY_STRING
TEST.UNIT:tzUtilLoggerA
TEST.SUBPROGRAM:findModuleIdByNameA
TEST.NEW
TEST.NAME:findModuleIdByNameA_EMPTY_STRING
TEST.NOTES:
<userTags> 
<History> 
2022-03-25	Rafal Gerhand   	Initail creation in CBR2_TZ_220210_1
</History> 
  
<Description> 
This test case verifies the execution of function findModuleIdByNameA() when:
-value of the "name" parameter is a empty string
-moduleId is allocated
</Description> 

<Prerequisites> 
None
</Prerequisites> 
  
<Given> 
As per the EQUIVALENCE CLASS
</Given> 
  
<Expected> 
Verify that:
-moduleId[0] is equal to TZ_MODULE_ID_ANY
-Return of findModuleIdByNameA() is equal to 1
</Expected> 
  
<Requirement> 
None 
</Requirement> 
  
<EquivalenceClass> 
Refer to Equivalence Class-Boundary Value: findModuleIdByNameA02 and findModuleIdByNameA04 of function findModuleIdByNameA in Component_Test_Specification_TZUTIL.docx
</EquivalenceClass> 
  
<Usage> 
Boundary
</Usage> 
</userTags> 
TEST.END_NOTES:
TEST.VALUE:tzUtilLoggerA.findModuleIdByNameA.name:<<malloc 1>>
TEST.VALUE:tzUtilLoggerA.findModuleIdByNameA.name:""
TEST.VALUE:tzUtilLoggerA.findModuleIdByNameA.moduleId:<<malloc 1>>
TEST.EXPECTED:tzUtilLoggerA.findModuleIdByNameA.moduleId[0]:TZ_MODULE_ID_ANY
TEST.EXPECTED:tzUtilLoggerA.findModuleIdByNameA.return:1
TEST.END

-- Test Case: findModuleIdByNameA_NULL
TEST.UNIT:tzUtilLoggerA
TEST.SUBPROGRAM:findModuleIdByNameA
TEST.NEW
TEST.NAME:findModuleIdByNameA_NULL
TEST.NOTES:
<userTags> 
<History> 
2022-03-25	Rafal Gerhand   	Initail creation in CBR2_TZ_220210_1
</History> 
  
<Description> 
This test case verifies the execution of function findModuleIdByNameA() when:
-value of the "name" parameter is equal to NULL
</Description> 

<Prerequisites> 
None
</Prerequisites> 
  
<Given> 
As per the EQUIVALENCE CLASS
</Given> 
  
<Expected> 
Verify that:
-Return of findModuleIdByNameA() is equal to 0
</Expected> 
  
<Requirement> 
None 
</Requirement> 
  
<EquivalenceClass> 
Refer to Equivalence Class-Boundary Value: findModuleIdByNameA03 of function findModuleIdByNameA in Component_Test_Specification_TZUTIL.docx
</EquivalenceClass> 
  
<Usage> 
Boundary
</Usage> 
</userTags> 
TEST.END_NOTES:
TEST.EXPECTED:tzUtilLoggerA.findModuleIdByNameA.return:0
TEST.VALUE_USER_CODE:tzUtilLoggerA.findModuleIdByNameA.name
<<tzUtilLoggerA.findModuleIdByNameA.name>> = ( NULL );
TEST.END_VALUE_USER_CODE:
TEST.END

-- Test Case: findModuleIdByNameA_name_max
TEST.UNIT:tzUtilLoggerA
TEST.SUBPROGRAM:findModuleIdByNameA
TEST.NEW
TEST.NAME:findModuleIdByNameA_name_max
TEST.NOTES:
<userTags> 
<History> 
2022-03-25	Rafal Gerhand   	Initail creation in CBR2_TZ_220210_1
</History> 
  
<Description> 
This test case verifies the execution of function findModuleIdByNameA() when:
-value of the "name" parameter is set to max
-moduleId is allocated and value of maduleId[0] is set to TZ_MODULE_ID_MAX
</Description> 

<Prerequisites> 
None
</Prerequisites> 
  
<Given> 
As per the EQUIVALENCE CLASS
</Given> 
  
<Expected> 
Verify that:
-Return of findModuleIdByNameA() is equal to 0
</Expected> 
  
<Requirement> 
None 
</Requirement> 
  
<EquivalenceClass> 
Refer to Equivalence Class-Boundary Value: findModuleIdByNameA02, findModuleIdByNameA05, findModuleIdByNameA07, findModuleIdByNameA08 and findModuleIdByNameA010 of function findModuleIdByNameA in Component_Test_Specification_TZUTIL.docx
</EquivalenceClass> 
  
<Usage> 
Boundary
</Usage> 
</userTags> 
TEST.END_NOTES:
TEST.VALUE:tzUtilLoggerA.findModuleIdByNameA.name:<<malloc 4>>
TEST.VALUE:tzUtilLoggerA.findModuleIdByNameA.name:"max"
TEST.VALUE:tzUtilLoggerA.findModuleIdByNameA.moduleId:<<malloc 1>>
TEST.VALUE:tzUtilLoggerA.findModuleIdByNameA.moduleId[0]:TZ_MODULE_ID_MAX
TEST.EXPECTED:tzUtilLoggerA.findModuleIdByNameA.return:0
TEST.END

-- Test Case: findModuleIdByNameA_name_util
TEST.UNIT:tzUtilLoggerA
TEST.SUBPROGRAM:findModuleIdByNameA
TEST.NEW
TEST.NAME:findModuleIdByNameA_name_util
TEST.NOTES:
<userTags> 
<History> 
2022-03-25	Rafal Gerhand   	Initail creation in CBR2_TZ_220210_1
</History> 
  
<Description> 
This test case verifies the execution of function findModuleIdByNameA() when:
-value of the "name" parameter is set to util
-moduleId is allocated
</Description> 

<Prerequisites> 
None
</Prerequisites> 
  
<Given> 
As per the EQUIVALENCE CLASS
</Given> 
  
<Expected> 
Verify that:
-value of moduleId[0] is set to TZ_MODULE_ID_uTIL
-Return of findModuleIdByNameA() is equal to 1
</Expected> 
  
<Requirement> 
None 
</Requirement> 
  
<EquivalenceClass> 
Refer to Equivalence Class-Boundary Value: findModuleIdByNameA02, findModuleIdByNameA05 and findModuleIdByNameA07 of function findModuleIdByNameA in Component_Test_Specification_TZUTIL.docx
</EquivalenceClass> 
  
<Usage> 
Boundary
</Usage> 
</userTags> 
TEST.END_NOTES:
TEST.VALUE:tzUtilLoggerA.findModuleIdByNameA.name:<<malloc 5>>
TEST.VALUE:tzUtilLoggerA.findModuleIdByNameA.name:"util"
TEST.VALUE:tzUtilLoggerA.findModuleIdByNameA.moduleId:<<malloc 1>>
TEST.EXPECTED:tzUtilLoggerA.findModuleIdByNameA.moduleId[0]:TZ_MODULE_ID_UTIL
TEST.EXPECTED:tzUtilLoggerA.findModuleIdByNameA.return:1
TEST.END

-- Subprogram: getTimeStr

-- Test Case: getTimeStr_pNow_NULL
TEST.UNIT:tzUtilLoggerA
TEST.SUBPROGRAM:getTimeStr
TEST.NEW
TEST.NAME:getTimeStr_pNow_NULL
TEST.NOTES:
<userTags> 
<History> 
2022-03-25	Rafal Gerhand   	Initail creation in CBR2_TZ_220210_1
</History> 
  
<Description> 
This test case verifies the execution of function getTimeStr() when:
-return of utilGetTimeofDayA() function is equal to NULL
</Description> 

<Prerequisites> 
None
</Prerequisites> 
  
<Given> 
As per the EQUIVALENCE CLASS
</Given> 
  
<Expected> 
Verify that:
-Return of getTimeStr() is equal to 0
</Expected> 
  
<Requirement> 
Functional 
</Requirement> 
  
<EquivalenceClass> 
Refer to Equivalence Class-Boundary Value: getTimeStr01 of function getTimeStr in Component_Test_Specification_TZUTIL.docx
</EquivalenceClass> 
  
<Usage> 
Functional
</Usage> 
</userTags> 
TEST.END_NOTES:
TEST.EXPECTED:tzUtilLoggerA.getTimeStr.return:0
TEST.STUB_VAL_USER_CODE:uut_prototype_stubs.utilGetTimeOfDayA.return
<<uut_prototype_stubs.utilGetTimeOfDayA.return>> = ( NULL );
TEST.END_STUB_VAL_USER_CODE:
TEST.END

-- Test Case: getTimeStr_written_0
TEST.UNIT:tzUtilLoggerA
TEST.SUBPROGRAM:getTimeStr
TEST.NEW
TEST.NAME:getTimeStr_written_0
TEST.NOTES:
<userTags> 
<History> 
2022-03-25	Rafal Gerhand   	Initail creation in CBR2_TZ_220210_1
</History> 
  
<Description> 
This test case verifies the execution of function getTimeStr() when:
-return of strftime() function is equal to 0
-return of utilGetTimeOfDayA() function is stubbed
</Description> 

<Prerequisites> 
None
</Prerequisites> 
  
<Given> 
As per the EQUIVALENCE CLASS
</Given> 
  
<Expected> 
Verify that:
-Return of getTimeStr() is equal to 0
</Expected> 
  
<Requirement> 
Functional 
</Requirement> 
  
<EquivalenceClass> 
Refer to Equivalence Class-Boundary Value: getTimeStr02 and getTimeStr05 of function getTimeStr in Component_Test_Specification_TZUTIL.docx
</EquivalenceClass> 
  
<Usage> 
Functional
</Usage> 
</userTags> 
TEST.END_NOTES:
TEST.STUB:uut_prototype_stubs.strftime
TEST.VALUE:uut_prototype_stubs.strftime.return:0
TEST.EXPECTED:tzUtilLoggerA.getTimeStr.return:0
TEST.STUB_VAL_USER_CODE:uut_prototype_stubs.utilGetTimeOfDayA.return
struct timeval
 {
    int32_t tv_sec;        /* Seconds.  */
    int32_t tv_usec;    /* Microseconds.  */
 };

struct timeval asd;
asd.tv_sec = 3600;
asd.tv_usec = 5;
<<uut_prototype_stubs.utilGetTimeOfDayA.return>> = ( &asd );
TEST.END_STUB_VAL_USER_CODE:
TEST.END

-- Test Case: getTimeStr_written_1
TEST.UNIT:tzUtilLoggerA
TEST.SUBPROGRAM:getTimeStr
TEST.NEW
TEST.NAME:getTimeStr_written_1
TEST.NOTES:
<userTags> 
<History> 
2022-03-25	Rafal Gerhand   	Initail creation in CBR2_TZ_220210_1
</History> 
  
<Description> 
This test case verifies the execution of function getTimeStr() when:
-return of strftime() function is equal to 1
-return of utilGetTimeOfDayA() function is stubbed
-value of the "buffSize" parameter is equal to 0 
</Description> 

<Prerequisites> 
None
</Prerequisites> 
  
<Given> 
As per the EQUIVALENCE CLASS
</Given> 
  
<Expected> 
Verify that:
-Return of getTimeStr() is equal to 0
</Expected> 
  
<Requirement> 
Functional 
</Requirement> 
  
<EquivalenceClass> 
Refer to Equivalence Class-Boundary Value: getTimeStr02, getTimeStr04, getTimeStr07  of function getTimeStr in Component_Test_Specification_TZUTIL.docx
</EquivalenceClass> 
  
<Usage> 
Functional
</Usage> 
</userTags> 
TEST.END_NOTES:
TEST.STUB:uut_prototype_stubs.strftime
TEST.VALUE:uut_prototype_stubs.strftime.return:1
TEST.VALUE:tzUtilLoggerA.getTimeStr.buffSize:0
TEST.EXPECTED:tzUtilLoggerA.getTimeStr.return:0
TEST.STUB_VAL_USER_CODE:uut_prototype_stubs.utilGetTimeOfDayA.return
struct timeval
 {
    int32_t tv_sec;        /* Seconds.  */
    int32_t tv_usec;    /* Microseconds.  */
 };

struct timeval asd;
asd.tv_sec = 59;
asd.tv_usec = 59;
<<uut_prototype_stubs.utilGetTimeOfDayA.return>> = ( &asd );
TEST.END_STUB_VAL_USER_CODE:
TEST.END

-- Test Case: getTimeStr_written_1_timeStr_101
TEST.UNIT:tzUtilLoggerA
TEST.SUBPROGRAM:getTimeStr
TEST.NEW
TEST.NAME:getTimeStr_written_1_timeStr_101
TEST.NOTES:
<userTags> 
<History> 
2022-03-25	Rafal Gerhand   	Initail creation in CBR2_TZ_220210_1
</History> 
  
<Description> 
This test case verifies the execution of function getTimeStr() when:
-return of strftime() function is equal to 1
-return of utilGetTimeOfDayA() function is stubbed
-value of the "timeStr" is allocated
-value of the "buffSize" parameter is equal to 101 
</Description> 

<Prerequisites> 
None
</Prerequisites> 
  
<Given> 
As per the EQUIVALENCE CLASS
</Given> 
  
<Expected> 
Verify that:
-Return of getTimeStr() is equal to 1
</Expected> 
  
<Requirement> 
Functional 
</Requirement> 
  
<EquivalenceClass> 
Refer to Equivalence Class-Boundary Value: getTimeStr02, getTimeStr04, getTimeStr06  of function getTimeStr in Component_Test_Specification_TZUTIL.docx
</EquivalenceClass> 
  
<Usage> 
Functional
</Usage> 
</userTags> 
TEST.END_NOTES:
TEST.STUB:uut_prototype_stubs.strftime
TEST.VALUE:uut_prototype_stubs.strftime.return:1
TEST.VALUE:tzUtilLoggerA.getTimeStr.timeStr:<<malloc 100>>
TEST.VALUE:tzUtilLoggerA.getTimeStr.buffSize:101
TEST.EXPECTED:tzUtilLoggerA.getTimeStr.return:1
TEST.STUB_VAL_USER_CODE:uut_prototype_stubs.utilGetTimeOfDayA.return
struct timeval
 {
    int32_t tv_sec;        /* Seconds.  */
    int32_t tv_usec;    /* Microseconds.  */
 };

struct timeval asd;
asd.tv_sec = 3600;
asd.tv_usec = 5;
<<uut_prototype_stubs.utilGetTimeOfDayA.return>> = ( &asd );
TEST.END_STUB_VAL_USER_CODE:
TEST.END

-- Subprogram: tzLogA

-- Test Case: tzLogA_TZ_LOG_LVL_DEBUG
TEST.UNIT:tzUtilLoggerA
TEST.SUBPROGRAM:tzLogA
TEST.NEW
TEST.NAME:tzLogA_TZ_LOG_LVL_DEBUG
TEST.NOTES:
<userTags> 
<History> 
2022-03-25	Rafal Gerhand   	Initail creation in CBR2_TZ_220210_1
</History> 
  
<Description> 
This test case verifies the execution of function tzLogA() when:
-return of the vfwGetSide function is set to VFW_A_SIDE
-return of the tzLogGetLevelA function is set to TZ_MODULE_ID_ANY
-value of the g_isLoggingAllowed parameter is equal to 1
-value of the level parameter of tzLogA function is set to TZ_LOG_LVL_DEBUG
-value of the line parameter is set to MIN and MAX
</Description> 

<Prerequisites> 
None
</Prerequisites> 
  
<Given> 
As per the EQUIVALENCE CLASS
</Given> 
  
<Expected> 
Verify that:
-value of logLevel is equal to TZ_LOG_LVL_DEBUG
-value of line is equal MIN and MAX	
</Expected> 
  
<Requirement> 
Functional 
</Requirement> 
  
<EquivalenceClass> 
Refer to Equivalence Class-Boundary Value: tzLogA03, tzLogA06, tzLogA07, tzLogA09, tzLogA13 and tzLogA15 of function tzLogA in Component_Test_Specification_TZUTIL.docx
</EquivalenceClass> 
  
<Usage> 
Functional
</Usage> 
</userTags> 
TEST.END_NOTES:
TEST.STUB:tzUtilLoggerA.tzLogGetLevelA
TEST.STUB:tzUtilLoggerA.tzUtilLogMessageA
TEST.VALUE:uut_prototype_stubs.vfwGetSide.return:VFW_A_SIDE
TEST.VALUE:tzUtilLoggerA.<<GLOBAL>>.g_isLoggingAllowed:1
TEST.VALUE:tzUtilLoggerA.tzLogGetLevelA.return:TZ_LOG_LVL_DEBUG
TEST.VALUE:tzUtilLoggerA.tzLogA.level:TZ_LOG_LVL_DEBUG
TEST.VALUE:tzUtilLoggerA.tzLogA.line:<<MIN>>,<<MAX>>
TEST.EXPECTED:tzUtilLoggerA.tzUtilLogMessageA.logLevel:TZ_LOG_LVL_DEBUG
TEST.EXPECTED:tzUtilLoggerA.tzUtilLogMessageA.line:<<MIN>>,<<MAX>>
TEST.END

-- Test Case: tzLogA_TZ_LOG_LVL_DEBUG_getTimeStr_1
TEST.UNIT:tzUtilLoggerA
TEST.SUBPROGRAM:tzLogA
TEST.NEW
TEST.NAME:tzLogA_TZ_LOG_LVL_DEBUG_getTimeStr_1
TEST.NOTES:
<userTags> 
<History> 
2022-03-25	Rafal Gerhand   	Initail creation in CBR2_TZ_220210_1
</History> 
  
<Description> 
This test case verifies the execution of function tzLogA() when:
-return of the vfwGetSide function is set to VFW_A_SIDE
-return of the tzLogGetLevelA function is set to TZ_MODULE_ID_ANY
-return of the getTimeStr function is equal to 1
-value of the g_isLoggingAllowed parameter is equal to 1
-value of the line parameter is set to MIN and MAX
</Description> 

<Prerequisites> 
None
</Prerequisites> 
  
<Given> 
As per the EQUIVALENCE CLASS
</Given> 
  
<Expected> 
Verify that:
-value of logLevel is equal to TZ_LOG_LVL_DEBUG
-value of line is equal MIN and MAX	
</Expected> 
  
<Requirement> 
Functional 
</Requirement> 
  
<EquivalenceClass> 
Refer to Equivalence Class-Boundary Value: tzLogA03, tzLogA06, tzLogA07, tzLogA09, tzLogA13 and tzLogA17 of function tzLogA in Component_Test_Specification_TZUTIL.docx
</EquivalenceClass> 
  
<Usage> 
Functional
</Usage> 
</userTags> 
TEST.END_NOTES:
TEST.STUB:tzUtilLoggerA.tzLogGetLevelA
TEST.STUB:tzUtilLoggerA.getTimeStr
TEST.STUB:tzUtilLoggerA.tzUtilLogMessageA
TEST.VALUE:uut_prototype_stubs.vfwGetSide.return:VFW_A_SIDE
TEST.VALUE:tzUtilLoggerA.<<GLOBAL>>.g_isLoggingAllowed:1
TEST.VALUE:tzUtilLoggerA.tzLogGetLevelA.return:TZ_LOG_LVL_DEBUG
TEST.VALUE:tzUtilLoggerA.tzLogA.level:TZ_LOG_LVL_DEBUG
TEST.VALUE:tzUtilLoggerA.tzLogA.line:<<MIN>>,<<MAX>>
TEST.VALUE:tzUtilLoggerA.getTimeStr.return:1
TEST.EXPECTED:tzUtilLoggerA.tzUtilLogMessageA.logLevel:TZ_LOG_LVL_DEBUG
TEST.EXPECTED:tzUtilLoggerA.tzUtilLogMessageA.line:<<MIN>>,<<MAX>>
TEST.END

-- Test Case: tzLogA_TZ_LOG_LVL_DEBUG_getTimeStr_1_g_messages_0
TEST.UNIT:tzUtilLoggerA
TEST.SUBPROGRAM:tzLogA
TEST.NEW
TEST.NAME:tzLogA_TZ_LOG_LVL_DEBUG_getTimeStr_1_g_messages_0
TEST.NOTES:
<userTags> 
<History> 
2022-03-25	Rafal Gerhand   	Initail creation in CBR2_TZ_220210_1
</History> 
  
<Description> 
This test case verifies the execution of function tzLogA() when:
-return of the vfwGetSide function is set to VFW_A_SIDE
-return of the tzLogGetLevelA function is set to TZ_LOG_LVL_DEBUG
-return of the getTimeStr function is equal to 1
-value of the g_messages parameter is equal to 0
-value of the g_isLoggingAllowed parameter is equal to 1
-value of the level parameter is set to TZ_LOG_LVL_DEGUG
-velue of the message parameter is equal to 0
-value of the line parameter is set to MIN and MAX
</Description> 

<Prerequisites> 
None
</Prerequisites> 
  
<Given> 
As per the EQUIVALENCE CLASS
</Given> 
  
<Expected> 
Verify that:
-value of logLevel is equal to TZ_LOG_LVL_DEBUG
-value of line is equal MIN and MAX	
</Expected> 
  
<Requirement> 
Functional 
</Requirement> 
  
<EquivalenceClass> 
Refer to Equivalence Class-Boundary Value: tzLogA03, tzLogA06, tzLogA07, tzLogA09, tzLogA14 of function tzLogA in Component_Test_Specification_TZUTIL.docx
</EquivalenceClass> 
  
<Usage> 
Functional
</Usage> 
</userTags> 
TEST.END_NOTES:
TEST.STUB:tzUtilLoggerA.tzLogGetLevelA
TEST.STUB:tzUtilLoggerA.getTimeStr
TEST.STUB:tzUtilLoggerA.tzUtilLogMessageA
TEST.VALUE:uut_prototype_stubs.vfwGetSide.return:VFW_A_SIDE
TEST.VALUE:tzUtilLoggerA.<<GLOBAL>>.g_isLoggingAllowed:1
TEST.VALUE:tzUtilLoggerA.tzLogGetLevelA.return:TZ_LOG_LVL_DEBUG
TEST.VALUE:tzUtilLoggerA.tzLogA.level:TZ_LOG_LVL_DEBUG
TEST.VALUE:tzUtilLoggerA.tzLogA.line:<<MIN>>,<<MAX>>
TEST.VALUE:tzUtilLoggerA.getTimeStr.return:1
TEST.EXPECTED:tzUtilLoggerA.tzUtilLogMessageA.logLevel:TZ_LOG_LVL_DEBUG
TEST.EXPECTED:tzUtilLoggerA.tzUtilLogMessageA.line:<<MIN>>,<<MAX>>
TEST.VALUE_USER_CODE:tzUtilLoggerA.<<GLOBAL>>.g_messages
char **ptr = &<<tzUtilLoggerA.<<GLOBAL>>.g_messages>>; 
*ptr = 0;

TEST.END_VALUE_USER_CODE:
TEST.VALUE_USER_CODE:tzUtilLoggerA.tzLogA.message
<<tzUtilLoggerA.tzLogA.message>> = ( 0 );
TEST.END_VALUE_USER_CODE:
TEST.END

-- Test Case: tzLogA_TZ_MODULE_ID_ANY_TZ_LOG_LVL_ALERT
TEST.UNIT:tzUtilLoggerA
TEST.SUBPROGRAM:tzLogA
TEST.NEW
TEST.NAME:tzLogA_TZ_MODULE_ID_ANY_TZ_LOG_LVL_ALERT
TEST.NOTES:
<userTags> 
<History> 
2022-03-25	Rafal Gerhand   	Initail creation in CBR2_TZ_220210_1
</History> 
  
<Description> 
This test case verifies the execution of function tzLogA() when:
-return of the vfwGetSide function is set to VFW_A_SIDE
-value of the moduleId parameter of tzLogGetLevelA function is set to TZ_MODULE_ID_ANY
-value of the level parameter is set to TZ_LOG_LVL_ALERT
-value of the g_isLoggingAllowed parameter is equal to 1
</Description> 

<Prerequisites> 
None
</Prerequisites> 
  
<Given> 
As per the EQUIVALENCE CLASS
</Given> 
  
<Expected> 
Verify that:
-Return of assert_x is equal to 0
	
</Expected> 
  
<Requirement> 
Functional 
</Requirement> 
  
<EquivalenceClass> 
Refer to Equivalence Class-Boundary Value: tzLogA03, tzLogA06, tzLogA08 of function tzLogA in Component_Test_Specification_TZUTIL.docx
</EquivalenceClass> 
  
<Usage> 
Functional
</Usage> 
</userTags> 
TEST.END_NOTES:
TEST.STUB:tzUtilLoggerA.tzLogGetLevelA
TEST.VALUE:uut_prototype_stubs.vfwGetSide.return:VFW_A_SIDE
TEST.VALUE:tzUtilLoggerA.<<GLOBAL>>.g_isLoggingAllowed:1
TEST.VALUE:tzUtilLoggerA.tzLogGetLevelA.moduleId:TZ_MODULE_ID_ANY
TEST.VALUE:tzUtilLoggerA.tzLogA.level:TZ_LOG_LVL_ALERT
TEST.EXPECTED:tzUtilLoggerA.<<GLOBAL>>.assert_x:0
TEST.END

-- Test Case: tzLogA_TZ_MODULE_ID_ANY_TZ_MODULE_LVL_EMERG
TEST.UNIT:tzUtilLoggerA
TEST.SUBPROGRAM:tzLogA
TEST.NEW
TEST.NAME:tzLogA_TZ_MODULE_ID_ANY_TZ_MODULE_LVL_EMERG
TEST.NOTES:
<userTags> 
<History> 
2022-03-25	Rafal Gerhand   	Initail creation in CBR2_TZ_220210_1
</History> 
  
<Description> 
This test case verifies the execution of function tzLogA() when:
-return of the vfwGetSide function is set to VFW_A_SIDE
-value of the moduleId parameter of tzLogGetLevelA function is set to TZ_MODULE_ID_ANY
-return of the tzLogGetLevelA function is set to TZ_MODULE_ID_ANY
-value of the g_isLoggingAllowed parameter is equal to 0
-value of the line parameter is set to MIN and MAX
</Description> 

<Prerequisites> 
None
</Prerequisites> 
  
<Given> 
As per the EQUIVALENCE CLASS
</Given> 
  
<Expected> 
Verify that:
-value of logLevel is equal to TZ_LOG_LVL_EMERG
-value of line is equal MIN and MAX	
</Expected> 
  
<Requirement> 
Functional 
</Requirement> 
  
<EquivalenceClass> 
Refer to Equivalence Class-Boundary Value: tzLogA03, tzLogA06, tzLogA07 and tzLogA011 of function tzLogA in Component_Test_Specification_TZUTIL.docx
</EquivalenceClass> 
  
<Usage> 
Functional
</Usage> 
</userTags> 
TEST.END_NOTES:
TEST.STUB:tzUtilLoggerA.tzLogGetLevelA
TEST.STUB:tzUtilLoggerA.tzUtilLogMessageA
TEST.VALUE:uut_prototype_stubs.vfwGetSide.return:VFW_A_SIDE
TEST.VALUE:tzUtilLoggerA.<<GLOBAL>>.g_isLoggingAllowed:1
TEST.VALUE:tzUtilLoggerA.tzLogGetLevelA.moduleId:TZ_MODULE_ID_ANY
TEST.VALUE:tzUtilLoggerA.tzLogGetLevelA.return:TZ_LOG_LVL_EMERG
TEST.VALUE:tzUtilLoggerA.tzLogA.line:<<MIN>>,<<MAX>>
TEST.EXPECTED:tzUtilLoggerA.tzUtilLogMessageA.logLevel:TZ_LOG_LVL_EMERG
TEST.EXPECTED:tzUtilLoggerA.tzUtilLogMessageA.line:<<MIN>>,<<MAX>>
TEST.END

-- Test Case: tzLogA_VFW_NO_SIDE
TEST.UNIT:tzUtilLoggerA
TEST.SUBPROGRAM:tzLogA
TEST.NEW
TEST.NAME:tzLogA_VFW_NO_SIDE
TEST.NOTES:
<userTags> 
<History> 
2022-03-25	Rafal Gerhand   	Initail creation in CBR2_TZ_220210_1
</History> 
  
<Description> 
This test case verifies the execution of function tzLogA() when:
-return of vfwGetSide() function is set to VFW_NO_SIDE
-value of the g_isLoggingAllowed parameter is equal to 1
</Description> 

<Prerequisites> 
None
</Prerequisites> 
  
<Given> 
As per the EQUIVALENCE CLASS
</Given> 
  
<Expected> 
Verify that:
-Return of assert_x is equal to 0
</Expected> 
  
<Requirement> 
Functional 
</Requirement> 
  
<EquivalenceClass> 
Refer to Equivalence Class-Boundary Value: tzLogA03 and tzLogA04 of function tzLogA in Component_Test_Specification_TZUTIL.docx
</EquivalenceClass> 
  
<Usage> 
Functional
</Usage> 
</userTags> 
TEST.END_NOTES:
TEST.VALUE:uut_prototype_stubs.vfwGetSide.return:VFW_NO_SIDE
TEST.VALUE:tzUtilLoggerA.<<GLOBAL>>.g_isLoggingAllowed:1
TEST.EXPECTED:tzUtilLoggerA.<<GLOBAL>>.assert_x:0
TEST.END

-- Test Case: tzLogA_g_isLoggingAllowed_0
TEST.UNIT:tzUtilLoggerA
TEST.SUBPROGRAM:tzLogA
TEST.NEW
TEST.NAME:tzLogA_g_isLoggingAllowed_0
TEST.NOTES:
<userTags> 
<History> 
2022-03-25	Rafal Gerhand   	Initail creation in CBR2_TZ_220210_1
</History> 
  
<Description> 
This test case verifies the execution of function tzLogA() when:
-value of the g_isLoggingAllowed parameter is equal to 0
</Description> 

<Prerequisites> 
None
</Prerequisites> 
  
<Given> 
As per the EQUIVALENCE CLASS
</Given> 
  
<Expected> 
Verify that:
-Return of assert_x is equal to 0
</Expected> 
  
<Requirement> 
Functional 
</Requirement> 
  
<EquivalenceClass> 
Refer to Equivalence Class-Boundary Value: tzLogA01 of function tzLogA in Component_Test_Specification_TZUTIL.docx
</EquivalenceClass> 
  
<Usage> 
Functional
</Usage> 
</userTags> 
TEST.END_NOTES:
TEST.VALUE:tzUtilLoggerA.<<GLOBAL>>.g_isLoggingAllowed:0
TEST.EXPECTED:tzUtilLoggerA.<<GLOBAL>>.assert_x:0
TEST.END

-- Subprogram: tzLogChangedILObject

-- Test Case: tzLogChangedILObject_V424Object_NULL
TEST.UNIT:tzUtilLoggerA
TEST.SUBPROGRAM:tzLogChangedILObject
TEST.NEW
TEST.NAME:tzLogChangedILObject_V424Object_NULL
TEST.NOTES:
<userTags> 
<History> 
2022-03-25	Rafal Gerhand   	Initail creation in CBR2_TZ_220210_1
</History> 
  
<Description> 
This test case verifies the execution of function tzLogChangedILObject() when:
-value of the V424Object parameter is equal to NULL
</Description> 

<Prerequisites> 
None
</Prerequisites> 
  
<Given> 
As per the EQUIVALENCE CLASS
</Given> 
  
<Expected> 
Verify that:
-Return of assert_x is equal to 1
</Expected> 
  
<Requirement> 
Functional 
</Requirement> 
  
<EquivalenceClass> 
Refer to Equivalence Class-Boundary Value: tzLogChangedILObject02 of function tzLogChangedILObject in Component_Test_Specification_TZUTIL.docx
</EquivalenceClass> 
  
<Usage> 
Functional
</Usage> 
</userTags> 
TEST.END_NOTES:
TEST.VALUE:tzUtilLoggerA.tzLogChangedILObject.V424Object:<<null>>
TEST.EXPECTED:tzUtilLoggerA.<<GLOBAL>>.assert_x:1
TEST.END

-- Test Case: tzLogChangedILObject_VFW_A_SIDE
TEST.UNIT:tzUtilLoggerA
TEST.SUBPROGRAM:tzLogChangedILObject
TEST.NEW
TEST.NAME:tzLogChangedILObject_VFW_A_SIDE
TEST.NOTES:
<userTags> 
<History> 
2022-03-25	Rafal Gerhand   	Initail creation in CBR2_TZ_220210_1
</History> 
  
<Description> 
This test case verifies the execution of function tzLogChangedILObject() when:
-return of the vfwGetSide function is set to VFW_A_SIDE
-value of the V424Object parameter is allocated
</Description> 

<Prerequisites> 
None
</Prerequisites> 
  
<Given> 
As per the EQUIVALENCE CLASS
</Given> 
  
<Expected> 
Verify that:
-Return of assert_x is equal to 0
</Expected> 
  
<Requirement> 
Functional 
</Requirement> 
  
<EquivalenceClass> 
Refer to Equivalence Class-Boundary Value: tzLogChangedILObject01 and tzLogChangedILObject05 of function tzLogChangedILObject in Component_Test_Specification_TZUTIL.docx
</EquivalenceClass> 
  
<Usage> 
Functional
</Usage> 
</userTags> 
TEST.END_NOTES:
TEST.VALUE:uut_prototype_stubs.vfwGetSide.return:VFW_A_SIDE
TEST.VALUE:tzUtilLoggerA.tzLogChangedILObject.V424Object:<<malloc 1>>
TEST.FLOW
  tzUtilLoggerA.c.tzLogChangedILObject
  uut_prototype_stubs.vfwGetSide
  uut_prototype_stubs.vfwInitBuffer
  uut_prototype_stubs.vfwPutString
  uut_prototype_stubs.vfwPutU16
  uut_prototype_stubs.vfwPutU16
  uut_prototype_stubs.vfwPutI32
  uut_prototype_stubs.vfwPutU32
  uut_prototype_stubs.vfwPutU32
  uut_prototype_stubs.vfwPutU32
  uut_prototype_stubs.vfwPutU32
  uut_prototype_stubs.vfwPutU16
  uut_prototype_stubs.vfwPutU16
  uut_prototype_stubs.vfwGetValidSize
  uut_prototype_stubs.utilLogSendA
  tzUtilLoggerA.c.tzLogChangedILObject
TEST.END_FLOW
TEST.END

-- Test Case: tzLogChangedILObject_VFW_B_SIDE
TEST.UNIT:tzUtilLoggerA
TEST.SUBPROGRAM:tzLogChangedILObject
TEST.NEW
TEST.NAME:tzLogChangedILObject_VFW_B_SIDE
TEST.NOTES:
<userTags> 
<History> 
2022-03-25	Rafal Gerhand   	Initail creation in CBR2_TZ_220210_1
</History> 
  
<Description> 
This test case verifies the execution of function tzLogChangedILObject() when:
-return of the vfwGetSide function is set to VFW_NO_SIDE
-value of the V424Object parameter is allocated
</Description> 

<Prerequisites> 
None
</Prerequisites> 
  
<Given> 
As per the EQUIVALENCE CLASS
</Given> 
  
<Expected> 
Verify that:
-Return of assert_x is equal to 0
</Expected> 
  
<Requirement> 
Functional 
</Requirement> 
  
<EquivalenceClass> 
Refer to Equivalence Class-Boundary Value: tzLogChangedILObject01 and tzLogChangedILObject04 of function tzLogChangedILObject in Component_Test_Specification_TZUTIL.docx
</EquivalenceClass> 
  
<Usage> 
Functional
</Usage> 
</userTags> 
TEST.END_NOTES:
TEST.VALUE:uut_prototype_stubs.vfwGetSide.return:VFW_B_SIDE
TEST.VALUE:tzUtilLoggerA.tzLogChangedILObject.V424Object:<<malloc 1>>
TEST.EXPECTED:tzUtilLoggerA.<<GLOBAL>>.assert_x:0
TEST.END

-- Test Case: tzLogChangedILObject_VFW_NO_SIDE
TEST.UNIT:tzUtilLoggerA
TEST.SUBPROGRAM:tzLogChangedILObject
TEST.NEW
TEST.NAME:tzLogChangedILObject_VFW_NO_SIDE
TEST.NOTES:
<userTags> 
<History> 
2022-03-25	Rafal Gerhand   	Initail creation in CBR2_TZ_220210_1
</History> 
  
<Description> 
This test case verifies the execution of function tzLogChangedILObject() when:
-return of the vfwGetSide function is set to VFW_NO_SIDE
-value of the V424Object parameter is allocated
</Description> 

<Prerequisites> 
None
</Prerequisites> 
  
<Given> 
As per the EQUIVALENCE CLASS
</Given> 
  
<Expected> 
Verify that:
-Return of assert_x is equal to 0
</Expected> 
  
<Requirement> 
Functional 
</Requirement> 
  
<EquivalenceClass> 
Refer to Equivalence Class-Boundary Value: tzLogChangedILObject01 and tzLogChangedILObject03 of function tzLogChangedILObject in Component_Test_Specification_TZUTIL.docx
</EquivalenceClass> 
  
<Usage> 
Functional
</Usage> 
</userTags> 
TEST.END_NOTES:
TEST.VALUE:uut_prototype_stubs.vfwGetSide.return:VFW_NO_SIDE
TEST.VALUE:tzUtilLoggerA.tzLogChangedILObject.V424Object:<<malloc 1>>
TEST.EXPECTED:tzUtilLoggerA.<<GLOBAL>>.assert_x:0
TEST.END

-- Subprogram: tzLogGetLevelA

-- Test Case: tzLogGetLevelA_moduleId_TZ_MODULE_ID_ANY
TEST.UNIT:tzUtilLoggerA
TEST.SUBPROGRAM:tzLogGetLevelA
TEST.NEW
TEST.NAME:tzLogGetLevelA_moduleId_TZ_MODULE_ID_ANY
TEST.NOTES:
<userTags> 
<History> 
2020-11-16	Konrad Terlikowski	Initail creation 
</History> 
  
<Description> 
This test case verifies proper behaviour when moduleId is set to TZ_MODULE_ID_ANY
</Description> 

<Prerequisites> 
None
</Prerequisites> 
  
<Given> 
- moduleID is set to TZ_MODULE_ID_ANY
</Given> 
  
<Expected> 
- assert_x should be incremented
</Expected> 
  
<Requirement> 
Functional 
</Requirement> 
  
<EquivalenceClass> 
Refer to Equivalence Class Value:T.001-EQ.001-BV.001 of function tzLogGetLevelA in ***
</EquivalenceClass> 
  
<Usage> 
Functional
</Usage> 
</userTags> 
TEST.END_NOTES:
TEST.VALUE:tzUtilLoggerA.tzLogGetLevelA.moduleId:TZ_MODULE_ID_ANY
TEST.EXPECTED:tzUtilLoggerA.<<GLOBAL>>.assert_x:1
TEST.END

-- Test Case: tzLogGetLevelA_moduleId_TZ_MODULE_ID_MAX
TEST.UNIT:tzUtilLoggerA
TEST.SUBPROGRAM:tzLogGetLevelA
TEST.NEW
TEST.NAME:tzLogGetLevelA_moduleId_TZ_MODULE_ID_MAX
TEST.NOTES:
<userTags> 
<History> 
2020-11-16	Konrad Terlikowski	Initail creation 
</History> 
  
<Description> 
This test case verifies proper behaviour when moduleId is set to TZ_MODULE_ID_MAX
</Description> 

<Prerequisites> 
None
</Prerequisites> 
  
<Given> 
- moduleID is set to TZ_MODULE_ID_MAX
</Given> 
  
<Expected> 
- assert_x should be incremented
</Expected> 
  
<Requirement> 
Functional 
</Requirement> 
  
<EquivalenceClass> 
Refer to Equivalence Class Value:T.001-EQ.001-BV.002 of function tzLogGetLevelA in ***
</EquivalenceClass> 
  
<Usage> 
Functional
</Usage> 
</userTags> 
TEST.END_NOTES:
TEST.VALUE:tzUtilLoggerA.tzLogGetLevelA.moduleId:TZ_MODULE_ID_MAX
TEST.EXPECTED:tzUtilLoggerA.<<GLOBAL>>.assert_x:1
TEST.END

-- Test Case: tzLogGetLevelA_moduleId_between_TZ_MODULE_ID_ANY_and_TZ_MODULE_ID_MAX
TEST.UNIT:tzUtilLoggerA
TEST.SUBPROGRAM:tzLogGetLevelA
TEST.NEW
TEST.NAME:tzLogGetLevelA_moduleId_between_TZ_MODULE_ID_ANY_and_TZ_MODULE_ID_MAX
TEST.NOTES:
<userTags> 
<History> 
2020-11-16	Konrad Terlikowski	Initail creation 
</History> 
  
<Description> 
This test case verifies proper behaviour when moduleId is between TZ_MODULE_ID_ANY and TZ_MODULE_ID_MAX
</Description> 

<Prerequisites> 
None
</Prerequisites> 
  
<Given> 
- moduleID is set to TZ_MODULE_ID_LOGIC
</Given> 
  
<Expected> 
- assert_x should not be incremented
</Expected> 
  
<Requirement> 
Functional 
</Requirement> 
  
<EquivalenceClass> 
Refer to Equivalence Class Value:T.002-EQ.002-BV.003 of function tzLogGetLevelA in ***
</EquivalenceClass> 
  
<Usage> 
Functional
</Usage> 
</userTags> 
TEST.END_NOTES:
TEST.VALUE:tzUtilLoggerA.tzLogGetLevelA.moduleId:TZ_MODULE_ID_LOGIC
TEST.EXPECTED:tzUtilLoggerA.<<GLOBAL>>.assert_x:0
TEST.END

-- Subprogram: tzLogSetLevelA

-- Test Case: tzLogSetLevelA_moduleId_TZ_MODULE_ID_ANY
TEST.UNIT:tzUtilLoggerA
TEST.SUBPROGRAM:tzLogSetLevelA
TEST.NEW
TEST.NAME:tzLogSetLevelA_moduleId_TZ_MODULE_ID_ANY
TEST.NOTES:
<userTags> 
<History> 
2022-03-25	Rafal Gerhand   	Initail creation in CBR2_TZ_220210_1
</History> 
  
<Description> 
This test case verifies the execution of function tzLogSetLevelA() when:
-value of the moduleId parameter is set to TZ_MODULE_ID_ANY
</Description> 

<Prerequisites> 
None
</Prerequisites> 
  
<Given> 
As per the EQUIVALENCE CLASS
</Given> 
  
<Expected> 
Verify that:
-Return of the level parameter is equal to TZ_LOG_LVL_NOTICE
-Return of the moduleId parameter is equal to TZ_MODULE_ID_UTIL
-Return of the message parameter is equal to TZ_LOG_MESSAGE_LOG_LEVEL_CHANGED
-Return of the param parameter is equal to TZ_LOG_MESSAGE_LOG_LEVEL_CHANGED:
New log level for module all is 0,New log level for module cbi is 0,New log level for module logic is 0,New log level for module psr is 0,New log level for module util is 0,New log level for module vmmi is 0
-Return of the function parameter is equal to tzLogSetLevelA
-Return of the line parameter is equal to 188
</Expected> 
  
<Requirement> 
Functional 
</Requirement> 
  
<EquivalenceClass> 
Refer to Equivalence Class-Boundary Value: tzLogSetLevelA01, tzLogSetLevelA04 and tzLogSetLevelA05 of function tzLogSetLevelA in Component_Test_Specification_TZUTIL.docx
</EquivalenceClass> 
  
<Usage> 
Functional
</Usage> 
</userTags> 
TEST.END_NOTES:
TEST.STUB:tzUtilLoggerA.tzLogA
TEST.VALUE:tzUtilLoggerA.tzLogSetLevelA.moduleId:TZ_MODULE_ID_ANY
TEST.EXPECTED:tzUtilLoggerA.tzLogA.level:TZ_LOG_LVL_NOTICE
TEST.EXPECTED:tzUtilLoggerA.tzLogA.moduleId:TZ_MODULE_ID_UTIL
TEST.EXPECTED:tzUtilLoggerA.tzLogA.message:TZ_LOG_MESSAGE_LOG_LEVEL_CHANGED
TEST.EXPECTED:tzUtilLoggerA.tzLogA.param:"New log level for module all is 0","New log level for module cbi is 0","New log level for module logic is 0","New log level for module psr is 0","New log level for module util is 0","New log level for module vmmi is 0"
TEST.EXPECTED:tzUtilLoggerA.tzLogA.function:"tzLogSetLevelA"
TEST.EXPECTED:tzUtilLoggerA.tzLogA.line:188
TEST.END

-- Test Case: tzLogSetLevelA_moduleId_TZ_MODULE_ID_CBI
TEST.UNIT:tzUtilLoggerA
TEST.SUBPROGRAM:tzLogSetLevelA
TEST.NEW
TEST.NAME:tzLogSetLevelA_moduleId_TZ_MODULE_ID_CBI
TEST.NOTES:
<userTags> 
<History> 
2022-03-25	Rafal Gerhand   	Initail creation in CBR2_TZ_220210_1
</History> 
  
<Description> 
This test case verifies the execution of function tzLogSetLevelA() when:
-value of the moduleId parameter is set to TZ_MODULE_ID_CBI
-value of the level parameter is set to TZ_LOG_LVL_INFO
</Description> 

<Prerequisites> 
None
</Prerequisites> 
  
<Given> 
As per the EQUIVALENCE CLASS
</Given> 
  
<Expected> 
Verify that:
-Return of the level parameter is equal to TZ_LOG_LVL_NOTICE
-Return of the moduleId parameter is equal to TZ_MODULE_ID_UTIL
-Return of the message parameter is equal to TZ_LOG_MESSAGE_LOG_LEVEL_CHANGED
-Return of the param parameter is equal to New log level for module cbi is 6
-Return of the function parameter is equal to tzLogSetLevelA
-Return of the line parameter is equal to 196
</Expected> 
  
<Requirement> 
Functional 
</Requirement> 
  
<EquivalenceClass> 
Refer to Equivalence Class-Boundary Value: tzLogSetLevelA02, tzLogSetLevelA06, tzLogSetLevelA08, tzLogSetLevelA10 and tzLogSetLevelA12 of function tzLogSetLevelA in Component_Test_Specification_TZUTIL.docx
</EquivalenceClass> 
  
<Usage> 
Functional
</Usage> 
</userTags> 
TEST.END_NOTES:
TEST.STUB:tzUtilLoggerA.tzLogA
TEST.VALUE:tzUtilLoggerA.tzLogSetLevelA.moduleId:TZ_MODULE_ID_CBI
TEST.VALUE:tzUtilLoggerA.tzLogSetLevelA.level:TZ_LOG_LVL_INFO
TEST.EXPECTED:tzUtilLoggerA.tzLogA.level:TZ_LOG_LVL_NOTICE
TEST.EXPECTED:tzUtilLoggerA.tzLogA.moduleId:TZ_MODULE_ID_UTIL
TEST.EXPECTED:tzUtilLoggerA.tzLogA.message:TZ_LOG_MESSAGE_LOG_LEVEL_CHANGED
TEST.EXPECTED:tzUtilLoggerA.tzLogA.param:"New log level for module cbi is 6"
TEST.EXPECTED:tzUtilLoggerA.tzLogA.function:"tzLogSetLevelA"
TEST.EXPECTED:tzUtilLoggerA.tzLogA.line:196
TEST.END

-- Test Case: tzLogSetLevelA_moduleId_TZ_MODULE_ID_MAX
TEST.UNIT:tzUtilLoggerA
TEST.SUBPROGRAM:tzLogSetLevelA
TEST.NEW
TEST.NAME:tzLogSetLevelA_moduleId_TZ_MODULE_ID_MAX
TEST.NOTES:
<userTags> 
<History> 
2022-03-25	Rafal Gerhand   	Initail creation in CBR2_TZ_220210_1
</History> 
  
<Description> 
This test case verifies the execution of function tzLogSetLevelA() when:
-value of the moduleId parameter is set to TZ_MODULE_ID_MAX
</Description> 

<Prerequisites> 
None
</Prerequisites> 
  
<Given> 
As per the EQUIVALENCE CLASS
</Given> 
  
<Expected> 
Verify that:
-Return of the level parameter is equal to TZ_LOG_LVL_WARNING
-Return of the moduleId parameter is equal to TZ_MODULE_ID_UTIL
-Return of the message parameter is equal to TZ_LOG_MESSAGE_LOG_LEVEL_CHANGED
-Return of the param parameter is equal to Attempt to change log level failed\, module ID is invalid (6)
-Return of the function parameter is equal to tzLogSetLevelA
-Return of the line parameter is equal to 205
</Expected> 
  
<Requirement> 
Functional 
</Requirement> 
  
<EquivalenceClass> 
Refer to Equivalence Class-Boundary Value: tzLogSetLevelA02 and tzLogSetLevelA06 of function tzLogSetLevelA in Component_Test_Specification_TZUTIL.docx
</EquivalenceClass> 
  
<Usage> 
Functional
</Usage> 
</userTags> 
TEST.END_NOTES:
TEST.STUB:tzUtilLoggerA.tzLogA
TEST.VALUE:tzUtilLoggerA.tzLogSetLevelA.moduleId:TZ_MODULE_ID_MAX
TEST.EXPECTED:tzUtilLoggerA.tzLogA.level:TZ_LOG_LVL_WARNING
TEST.EXPECTED:tzUtilLoggerA.tzLogA.moduleId:TZ_MODULE_ID_UTIL
TEST.EXPECTED:tzUtilLoggerA.tzLogA.message:TZ_LOG_MESSAGE_LOG_LEVEL_CHANGED
TEST.EXPECTED:tzUtilLoggerA.tzLogA.param:"Attempt to change log level failed, module ID is invalid (6)"
TEST.EXPECTED:tzUtilLoggerA.tzLogA.function:"tzLogSetLevelA"
TEST.EXPECTED:tzUtilLoggerA.tzLogA.line:205
TEST.END

-- Test Case: tzLogSetLevelA_moduleId_TZ_MODULE_ID_MAX_and_level_-1
TEST.UNIT:tzUtilLoggerA
TEST.SUBPROGRAM:tzLogSetLevelA
TEST.NEW
TEST.NAME:tzLogSetLevelA_moduleId_TZ_MODULE_ID_MAX_and_level_-1
TEST.NOTES:
<userTags> 
<History> 
2022-03-25	Rafal Gerhand   	Initail creation in CBR2_TZ_220210_1
</History> 
  
<Description> 
This test case verifies the execution of function tzLogSetLevelA() when:
-value of the moduleId parameter is set to TZ_MODULE_ID_CBI
-value of the level parameter is equal to -1
</Description> 

<Prerequisites> 
None
</Prerequisites> 
  
<Given> 
As per the EQUIVALENCE CLASS
</Given> 
  
<Expected> 
Verify that:
-Return of the level parameter is equal to TZ_LOG_LVL_WARNING
-Return of the moduleId parameter is equal to TZ_MODULE_ID_UTIL
-Return of the message parameter is equal to TZ_LOG_MESSAGE_LOG_LEVEL_CHANGED
-Return of the param parameter is equal to Attempt to change log level for module cbi failed\, level is invalid (-1)
-Return of the function parameter is equal to tzLogSetLevelA
-Return of the line parameter is equal to 200
</Expected> 
  
<Requirement> 
Functional 
</Requirement> 
  
<EquivalenceClass> 
Refer to Equivalence Class-Boundary Value: tzLogSetLevelA02, tzLogSetLevelA06, tzLogSetLevelA08, tzLogSetLevelA10 and tzLogSetLevelA13 of function tzLogSetLevelA in Component_Test_Specification_TZUTIL.docx
</EquivalenceClass> 
  
<Usage> 
Functional
</Usage> 
</userTags> 
TEST.END_NOTES:
TEST.STUB:tzUtilLoggerA.tzLogA
TEST.VALUE:tzUtilLoggerA.tzLogSetLevelA.moduleId:TZ_MODULE_ID_CBI
TEST.EXPECTED:tzUtilLoggerA.tzLogA.level:TZ_LOG_LVL_WARNING
TEST.EXPECTED:tzUtilLoggerA.tzLogA.moduleId:TZ_MODULE_ID_UTIL
TEST.EXPECTED:tzUtilLoggerA.tzLogA.message:TZ_LOG_MESSAGE_LOG_LEVEL_CHANGED
TEST.EXPECTED:tzUtilLoggerA.tzLogA.param:"Attempt to change log level for module cbi failed, level is invalid (-1)"
TEST.EXPECTED:tzUtilLoggerA.tzLogA.function:"tzLogSetLevelA"
TEST.EXPECTED:tzUtilLoggerA.tzLogA.line:200
TEST.VALUE_USER_CODE:tzUtilLoggerA.tzLogSetLevelA.level
<<tzUtilLoggerA.tzLogSetLevelA.level>> = ( -1);
TEST.END_VALUE_USER_CODE:
TEST.END

-- Test Case: tzLogSetLevelA_moduleId_TZ_MODULE_ID_MAX_and_level_8
TEST.UNIT:tzUtilLoggerA
TEST.SUBPROGRAM:tzLogSetLevelA
TEST.NEW
TEST.NAME:tzLogSetLevelA_moduleId_TZ_MODULE_ID_MAX_and_level_8
TEST.NOTES:
<userTags> 
<History> 
2022-03-25	Rafal Gerhand   	Initail creation in CBR2_TZ_220210_1
</History> 
  
<Description> 
This test case verifies the execution of function tzLogSetLevelA() when:
-value of the moduleId parameter is set to TZ_MODULE_ID_CBI
-value of the level parameter is equal to 8
</Description> 

<Prerequisites> 
None
</Prerequisites> 
  
<Given> 
As per the EQUIVALENCE CLASS
</Given> 
  
<Expected> 
Verify that:
-Return of the level parameter is equal to TZ_LOG_LVL_WARNING
-Return of the moduleId parameter is equal to TZ_MODULE_ID_UTIL
-Return of the message parameter is equal to TZ_LOG_MESSAGE_LOG_LEVEL_CHANGED
-Return of the param parameter is equal to Attempt to change log level for module cbi failed\, level is invalid (8)
-Return of the function parameter is equal to tzLogSetLevelA
-Return of the line parameter is equal to 200
</Expected> 
  
<Requirement> 
Functional 
</Requirement> 
  
<EquivalenceClass> 
Refer to Equivalence Class-Boundary Value: tzLogSetLevelA02, tzLogSetLevelA06, tzLogSetLevelA08, tzLogSetLevelA10 and tzLogSetLevelA13 of function tzLogSetLevelA in Component_Test_Specification_TZUTIL.docx
</EquivalenceClass> 
  
<Usage> 
Functional
</Usage> 
</userTags> 
TEST.END_NOTES:
TEST.STUB:tzUtilLoggerA.tzLogA
TEST.VALUE:tzUtilLoggerA.tzLogSetLevelA.moduleId:TZ_MODULE_ID_CBI
TEST.EXPECTED:tzUtilLoggerA.tzLogA.level:TZ_LOG_LVL_WARNING
TEST.EXPECTED:tzUtilLoggerA.tzLogA.moduleId:TZ_MODULE_ID_UTIL
TEST.EXPECTED:tzUtilLoggerA.tzLogA.message:TZ_LOG_MESSAGE_LOG_LEVEL_CHANGED
TEST.EXPECTED:tzUtilLoggerA.tzLogA.param:"Attempt to change log level for module cbi failed, level is invalid (8)"
TEST.EXPECTED:tzUtilLoggerA.tzLogA.function:"tzLogSetLevelA"
TEST.EXPECTED:tzUtilLoggerA.tzLogA.line:200
TEST.VALUE_USER_CODE:tzUtilLoggerA.tzLogSetLevelA.level
<<tzUtilLoggerA.tzLogSetLevelA.level>> = ( 8 );
TEST.END_VALUE_USER_CODE:
TEST.END

-- Subprogram: tzLogSetLevelStrA

-- Test Case: tzLogSetLevelStrA_findModuleIdByName_0
TEST.UNIT:tzUtilLoggerA
TEST.SUBPROGRAM:tzLogSetLevelStrA
TEST.NEW
TEST.NAME:tzLogSetLevelStrA_findModuleIdByName_0
TEST.NOTES:
<userTags> 
<History> 
2022-03-25	Rafal Gerhand   	Initail creation in CBR2_TZ_220210_1
</History> 
  
<Description> 
This test case verifies the execution of function tzLogSetLevelStrA() when:
-return of the findModuleIdByNameA function is equal to 0
</Description> 

<Prerequisites> 
None
</Prerequisites> 
  
<Given> 
As per the EQUIVALENCE CLASS
</Given> 
  
<Expected> 
Verify that:
-correct control flow
</Expected> 
  
<Requirement> 
Functional 
</Requirement> 
  
<EquivalenceClass> 
Refer to Equivalence Class-Boundary Value: tzLogSetLevelStrA02	 of function tzLogSetLevelStrA in Component_Test_Specification_TZUTIL.docx
</EquivalenceClass> 
  
<Usage> 
Functional
</Usage> 
</userTags> 
TEST.END_NOTES:
TEST.STUB:tzUtilLoggerA.findModuleIdByNameA
TEST.VALUE:tzUtilLoggerA.findModuleIdByNameA.return:0
TEST.FLOW
  tzUtilLoggerA.c.tzLogSetLevelStrA
  tzUtilLoggerA.c.findModuleIdByNameA
  tzUtilLoggerA.c.tzLogSetLevelStrA
TEST.END_FLOW
TEST.END

-- Test Case: tzLogSetLevelStrA_findModuleIdByName_1
TEST.UNIT:tzUtilLoggerA
TEST.SUBPROGRAM:tzLogSetLevelStrA
TEST.NEW
TEST.NAME:tzLogSetLevelStrA_findModuleIdByName_1
TEST.NOTES:
<userTags> 
<History> 
2022-03-25	Rafal Gerhand   	Initail creation in CBR2_TZ_220210_1
</History> 
  
<Description> 
This test case verifies the execution of function tzLogSetLevelA() when:
-return of the findModuleIdByNameA function is equal to 1
</Description> 

<Prerequisites> 
None
</Prerequisites> 
  
<Given> 
As per the EQUIVALENCE CLASS
</Given> 
  
<Expected> 
Verify that:
-correct control flow
</Expected> 
  
<Requirement> 
Functional 
</Requirement> 
  
<EquivalenceClass> 
Refer to Equivalence Class-Boundary Value: tzLogSetLevelStrA01 of function tzLogSetLevelStrA in Component_Test_Specification_TZUTIL.docx
</EquivalenceClass> 
  
<Usage> 
Functional
</Usage> 
</userTags> 
TEST.END_NOTES:
TEST.STUB:tzUtilLoggerA.findModuleIdByNameA
TEST.VALUE:tzUtilLoggerA.findModuleIdByNameA.return:1
TEST.FLOW
  tzUtilLoggerA.c.tzLogSetLevelStrA
  tzUtilLoggerA.c.findModuleIdByNameA
  tzUtilLoggerA.c.tzLogSetLevelStrA
TEST.END_FLOW
TEST.END

-- Subprogram: tzLogSetLoggingAllowedA

-- Test Case: tzLogSetLoggingAllowedA_isLoggingAllowed_1
TEST.UNIT:tzUtilLoggerA
TEST.SUBPROGRAM:tzLogSetLoggingAllowedA
TEST.NEW
TEST.NAME:tzLogSetLoggingAllowedA_isLoggingAllowed_1
TEST.NOTES:
<userTags> 
<History> 
2022-03-25	Rafal Gerhand   	Initail creation in CBR2_TZ_220210_1
</History> 
  
<Description> 
This test case verifies the execution of function tzLogSetLoggingAllowedA() when:
-value of the isLoggingAllowed parameter of tzLogSetLoggingAllowedA function is equal to 1
</Description> 

<Prerequisites> 
None
</Prerequisites> 
  
<Given> 
-isLoggingAllowed is equal to 1
</Given> 
  
<Expected> 
Verify that:
-value of the g_isLoggingAllowed parameter is equal to 1
</Expected> 
  
<Requirement> 
Functional 
</Requirement> 
  
<EquivalenceClass> 
None
</EquivalenceClass> 
  
<Usage> 
Functional
</Usage> 
</userTags> 
TEST.END_NOTES:
TEST.VALUE:tzUtilLoggerA.tzLogSetLoggingAllowedA.isLoggingAllowed:1
TEST.EXPECTED:tzUtilLoggerA.<<GLOBAL>>.g_isLoggingAllowed:1
TEST.END

-- Subprogram: tzUtilLogMessageA

-- Test Case: tzUtilLogMessageA_TZ_LOG_LVL_EMERG_TZ_LOG_MESSAGE_LOG_NO_MESSAGE
TEST.UNIT:tzUtilLoggerA
TEST.SUBPROGRAM:tzUtilLogMessageA
TEST.NEW
TEST.NAME:tzUtilLogMessageA_TZ_LOG_LVL_EMERG_TZ_LOG_MESSAGE_LOG_NO_MESSAGE
TEST.NOTES:
<userTags> 
<History> 
2022-03-25	Rafal Gerhand   	Initail creation in CBR2_TZ_220210_1
</History> 
  
<Description> 
This test case verifies the execution of function tzUtilLogMessageA() when:
-value of the logLevel parameter of tzUtilLogMessageA function is set to TZ_LOG_LVL_EMERG
-value of the message parameter of tzUtilLogMessageA function is set to TZ_LOG_MESSAGE_LOG_NO_MESSAGE
</Description> 

<Prerequisites> 
None
</Prerequisites> 
  
<Given> 
-isLoggingAllowed is equal to 1
</Given> 
  
<Expected> 
Verify that:
-value of the logLevel parameter of tzUtilLogPackA function is set to TZ_LOG_LVL_EMERG
-value of the message parameter of tzUtilLogPackA function is set to TZ_LOG_MESSAGE_LOG_NO_MESSAGE
</Expected> 
  
<Requirement> 
Functional 
</Requirement> 
  
<EquivalenceClass> 
None
</EquivalenceClass> 
  
<Usage> 
Functional
</Usage> 
</userTags> 
TEST.END_NOTES:
TEST.STUB:tzUtilLoggerA.tzUtilLogPackA
TEST.VALUE:tzUtilLoggerA.tzUtilLogMessageA.logLevel:TZ_LOG_LVL_EMERG
TEST.VALUE:tzUtilLoggerA.tzUtilLogMessageA.message:TZ_LOG_MESSAGE_LOG_NO_MESSAGE
TEST.EXPECTED:tzUtilLoggerA.tzUtilLogPackA.logLevel:TZ_LOG_LVL_EMERG
TEST.EXPECTED:tzUtilLoggerA.tzUtilLogPackA.message:TZ_LOG_MESSAGE_LOG_NO_MESSAGE
TEST.END

-- Subprogram: tzUtilLogPackA

-- Test Case: tzUtilLogPackA_TZ_LOG_LVL_ERR_TZ_LOG_MESSAGE_LOG_NO_MESSAGE
TEST.UNIT:tzUtilLoggerA
TEST.SUBPROGRAM:tzUtilLogPackA
TEST.NEW
TEST.NAME:tzUtilLogPackA_TZ_LOG_LVL_ERR_TZ_LOG_MESSAGE_LOG_NO_MESSAGE
TEST.NOTES:
<userTags> 
<History> 
2022-03-25	Rafal Gerhand   	Initail creation in CBR2_TZ_220210_1
</History> 
  
<Description> 
This test case verifies the execution of function tzUtilLogPackA() when:
-value of the logLevel parameter of tzUtilLogPackA function is set to TZ_LOG_LVL_ERR
-value of the message parameter of tzUtilLogPackA function is set to TZ_LOG_MESSAGE_LOG_NO_MESSAGE
</Description> 

<Prerequisites> 
None
</Prerequisites> 
  
<Given> 
-isLoggingAllowed is equal to 1
</Given> 
  
<Expected> 
Verify that:
-value of the value parameter of vfwPutI32 function is equal to 0 and 3
</Expected> 
  
<Requirement> 
Functional 
</Requirement> 
  
<EquivalenceClass> 
None
</EquivalenceClass> 
  
<Usage> 
Functional
</Usage> 
</userTags> 
TEST.END_NOTES:
TEST.VALUE:tzUtilLoggerA.tzUtilLogPackA.logLevel:TZ_LOG_LVL_ERR
TEST.VALUE:tzUtilLoggerA.tzUtilLogPackA.message:TZ_LOG_MESSAGE_LOG_NO_MESSAGE
TEST.EXPECTED:uut_prototype_stubs.vfwPutI32.value:0,3
TEST.END
